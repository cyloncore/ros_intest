import pytest
import subprocess

def pytest_collect_file(parent, file_path):
    if file_path.name.startswith("test_") and file_path.name.endswith(".ros_intest.yaml"):
        return RosIntestFile.from_parent(parent, path=file_path)

class RosIntestFile(pytest.File):
    def collect(self):
        import yaml

        raw = yaml.safe_load(self.path.open())
        yield RosIntestItem.from_parent(self, name=raw['name'])

class RosIntestItem(pytest.Item):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


    def runtest(self):
        cp = subprocess.run(["ros2", "run", "ros_intest", "run", self.path], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding="utf-8")
        if cp.returncode != 0:
            raise RosIntestException(cp.stdout)

    def repr_failure(self, excinfo):
        """Called when self.runtest() raises an exception."""
        if isinstance(excinfo.value, RosIntestException):
            return "\n".join(
                [
                    f"ros_intest '{self.path}' execution failed, with error:",
                    excinfo.value.args[0]
                ]
            )

    def reportinfo(self):
        return self.path, 0, f"usecase: {self.name}"


class RosIntestException(Exception):
    """Custom exception for error reporting."""