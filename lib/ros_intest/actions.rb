# Copyright 2024 Cyrille Berger.
#
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.

module RosIntest

  Actions = {}

  class Action
    def initialize
    end
    def prepare test_case
    end
    def execute test_case
    end
    def finish test_case
    end
  end

  class StartNode < Action
    def initialize package, executable, arguments, wait_for_finished
      @package = package
      @executable = executable
      @arguments = arguments
      @arguments = "" if @arguments.nil?
      @wait_for_finished = wait_for_finished
      @wait_for_finished = false if @wait_for_finished.nil?
    end
    def execute test_case
      begin
        pkg_prefix = Pkg.find_prefix @package
      rescue => e
        test_case.report_error e.message
        return
      end

      cmd = "#{pkg_prefix}/lib/#{@package}/#{@executable} #{@arguments}"
      puts "Starting node with command '#{cmd}'."
      @process = Utils::Process.new cmd
      if @wait_for_finished
        puts "...waiting for finished..."
        @process.wait_for_finished
      end
    end
    def finish test_case
      return if @process.nil?
      if @process.running?
        @process.terminate
      end
      if @process.status_code != 0
        test_case.report_error "Running #{@package} #{@executable} returned a non-null status code: #{@process.status_code} with output:\n#{@process.output}\n"
      end
      if test_case.options[:verbose]
        puts @process.output
      end
    end
    def StartNode.create_from_dict err_name, dict
      Utils.check_keys(err_name, dict, ["package", "executable"])
      return StartNode.new dict["package"], dict["executable"], dict["arguments"], dict["wait_for_finished"]
    end
  end

  Actions["start_node"] = StartNode

  class ExecuteLaunch < Action
    def initialize package, file, arguments
      @package = package
      @file = file
      @arguments = arguments
      @arguments = "" if @arguments.nil?
    end
    def execute test_case
      cmd = "ros2 launch #{@package} #{@file} #{@arguments}"
      puts "Launching with command '#{cmd}'."
      @process = Utils::Process.new cmd
    end
    def finish test_case
      if @process.running?
        @process.terminate
      elsif @process.status_code != 0
        test_case.report_error "Running #{@package} #{@file} returned a non-null status code: #{@process.status_code}"
      end
      if test_case.options[:verbose]
        puts @process.output
      end
    end
    def ExecuteLaunch.create_from_dict err_name, dict
      Utils.check_keys(err_name, dict, ["package", "file"])
      return ExecuteLaunch.new dict["package"], dict["file"], dict["arguments"]
    end
  end

  Actions["execute_launch"] = ExecuteLaunch

  class SendMessage < Action
    def SendMessage.create_from_dict err_name, dict
      return SendMessage.new
    end
  end

  Actions["send_message"] = SendMessage

  class MonitorTopic < Action
    def initialize name, type, publisher_count, frequency, message
      @name = name
      @type = type
      @publisher_count = publisher_count
      @frequency = frequency
      @message = message
      @last_time = nil
      @average_time = 0.0
      @average_time_count = 0
    end
    def prepare test_case
      splited_type = @type.split '/'
      require "#{splited_type[0]}/msg"
      msg_type = eval "#{Utils.camelize(splited_type[0])}::Msg::#{splited_type[2]}"
      @subscriber = test_case.ros_node.create_subscription(msg_type, @name, 1) do |msg|
        ct = Process.clock_gettime(Process::CLOCK_MONOTONIC)
        unless @last_time.nil?
          @average_time = @average_time + ct-@last_time
          @average_time_count += 1
        end
        @last_time = ct
        @message.check test_case, "msg", msg
      end
    end
    def finish test_case
      measured_frequency = @average_time_count / @average_time
      unless @frequency.nil?
        test_case.check_float "#{@name}: frequency (hz)", @frequency, measured_frequency, 0.001
      end
    end
    def MonitorTopic.create_from_dict err_name, dict
      Utils.check_keys(err_name, dict, ["name", "type"])
      return MonitorTopic.new dict["name"], dict["type"], dict["publisher_count"], dict["frequency"], StructureChecker.create_from_dict(dict["message"])
    end
  end

  Actions["monitor_topic"] = MonitorTopic

  class CallService < Action
    def initialize name, type, request, response, timeout_wait_for_service
      @name = name
      @type = type
      @request = request
      @request = {} if @request.nil?
      @response = response
      @timeout_wait_for_service = timeout_wait_for_service
      @timeout_wait_for_service = 10 if @timeout_wait_for_service.nil?
    end
    def prepare test_case
      splited_type = @type.split '/'
      require "#{splited_type[0]}/srv"
      @srv_type = eval "#{Utils.camelize(splited_type[0])}::Srv::#{splited_type[2]}"
      @client = test_case.ros_node.create_client @srv_type, @name
    end
    def execute test_case
      unless @client.wait_for_service timeout_sec = @timeout_wait_for_service
        test_case.report_error "Service #{@name} (#{@type}) is not available after waiting for #{@timeout_wait_for_service}s"
        return
      end
      puts "Calling #{@name}..."
      req = @srv_type::Request.create_from_dict @request
      result = @client.call_async(req).result
      puts "...got results."
      @response.check test_case, "result", result
    end
    def CallService.create_from_dict err_name, dict
      Utils.check_keys(err_name, dict, ["name", "type"])
      return CallService.new dict["name"], dict["type"], dict["request"], StructureChecker.create_from_dict(dict["response"]), dict["timeout/wait_for_service"]
    end
  end

  Actions["call_service"] = CallService

  class Wait < Action
    def initialize timeout
      @timeout = timeout
    end
    def execute test_case
      puts "Waiting for #{@timeout}s"
      sleep @timeout
    end
    def Wait.create_from_dict err_name, dict
      Utils.check_keys(err_name, dict, ["timeout"])
      return Wait.new dict["timeout"]
    end
  end

  Actions["wait"] = Wait

  class ActionLoader
    def ActionLoader.create_from_dict err_name, dict
      Utils.check_keys(err_name, dict, ["require", "class"])
      require dict["require"]
      klass = eval(dict["class"])
      return klass.create_from_dict err_name, dict["options"]
    end
  end

  Actions["action_loader"] = ActionLoader
end
