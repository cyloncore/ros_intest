# Copyright 2024 Cyrille Berger.
#
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.

require 'open3'

module RosIntest
  module Utils
    def Utils.check_keys err_name, dict, keys
      keys.each do |k|
        unless  dict.include? k
          raise StandardError.new "Missing key: #{k} in #{err_name}"
        end
      end
    end
    class Process
      attr_reader :output, :wait_thr
      def initialize cmd
        @input, @stdout, @wait_thr = Open3.popen2e(cmd)
        @output = ""
        @output_reader = Thread.new do
          until @stdout.eof?
            if IO.select [@stdout]
              @output += @stdout.read_nonblock 1024
            end
          end
        end
      end
      def terminate
        ::Process.kill("INT", @wait_thr.pid)
      end
      def running?
        return @wait_thr.status == "run" || @wait_thr.status == "sleep" 
      end
      def status_code
        return @wait_thr.value.exitstatus unless @wait_thr.value.nil?
        return 255
      end
      def wait_for_finished
        puts "Wait for #{@wait_thr.pid} to finish..."
        @wait_thr.value
      end
    end
    def Utils.camelize str
      return str.split("_").map(&:capitalize).join
    end
  end
end
