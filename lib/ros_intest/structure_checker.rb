# Copyright 2024 Cyrille Berger.
#
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.

module RosIntest
  module Internal
  end
  class RegexpChecker
    attr_reader :field_name
    def initialize field_name, regexp
      @field_name = field_name
      @regexp = regexp
    end
    def check test_case, name, value
      test_case.check_regexp name, @regexp, value
    end
  end
  class LiteralChecker
    attr_reader :field_name
    def initialize field_name, value
      @field_name = field_name
      @value = value
    end
    def check test_case, name, value
      test_case.check_equals name, @value, value
    end
  end
  class ArrayChecker
    attr_reader :field_name
    def initialize field_name, values_checkers
      @field_name = field_name
      @values_checkers = values_checkers
    end
    def check test_case, name, values
      test_case.check_equals name, @values_checkers.size, values.size
      if @values_checkers.size == values.size
        values.zip(@values_checkers, 0...values.size).each() do |v,c,i|
          c.check test_case, "#{name}[#{i}]", v
        end
      end
    end
  end
  class StructureChecker
    attr_reader :field_name
    def initialize field_name, fields
      @field_name = field_name
      @fields = fields
    end
    def check test_case, name, value
      @fields.each do |field|
        v = value.send(field.field_name.to_s)
        if v.nil?
          test_case.report_error "Missing field #{name}.#{field.field_name} in #{value}"
        else
          field.check test_case, "#{name}.#{field.field_name}", v
        end
      end
    end
    def StructureChecker.create_from_dict dict
      return StructureChecker.new nil, [] if dict.nil?
      return StructureChecker.new nil, StructureChecker.create_fields(dict)
    end
    def StructureChecker.create_fields dict
      fields = []
      dict.each do |k,v|
        fields.append(StructureChecker.create_field_checker k, v)
      end
      return fields
    end
    def StructureChecker.create_field_checker k, v
      if v.is_a? Regexp
        return RegexpChecker.new k, v
      elsif v.is_a? String or v.is_a? Float or v.is_a? Integer or [true, false].include? v
        return LiteralChecker.new k, v
      elsif v.is_a? Hash
        return StructureChecker.new(k, StructureChecker.create_fields(v))
      elsif v.is_a? Array
        return ArrayChecker.new(k, v.map { |x| StructureChecker.create_field_checker(nil, x) })
      else
        raise StandardError.new "Unhandle checker #{v}"
      end
    end
  end
end