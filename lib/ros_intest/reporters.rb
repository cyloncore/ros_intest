# Copyright 2024 Cyrille Berger.
#
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.

require 'rexml'

module RosIntest

  class JunitReporter
    def initialize filename
      @filename = filename
      @document = REXML::Document.new
      @document.add REXML::XMLDecl.new '1.0', 'utf-8'
      @testsuites = REXML::Element.new 'testsuites'
      @document.add @testsuites
    end
    def report_parse_error filename, err_msg
      current_suite = REXML::Element.new 'testsuite'
      current_suite.add_attribute 'name', filename
      @testsuites.add current_suite
      failure = REXML::Element.new 'failure'
      failure.add_attribute 'message', err_msg
      failure.add_attribute 'type', 'SyntaxError'
      current_suite.add failure
    end
    def start_case filename, name
      @current_suite = REXML::Element.new 'testsuite'
      @current_suite.add_attribute 'name', filename
      @testsuites.add @current_suite
      @current_case = REXML::Element.new 'testcase'
      @current_case.add_attribute 'name', name
      @current_suite.add @current_case
    end
    def report_error err_msg
      failure = REXML::Element.new 'failure'
      failure.add_attribute 'message', err_msg
      failure.add_attribute 'type', 'AssertionError'
      @current_case.add failure
    end
    def end_case time
      @current_suite.add_attribute 'time', time
      @current_case.add_attribute 'time', time
      @current_suite = nil
      @current_case = nil
    end
    def close time
      @testsuites.add_attribute 'time', time
      @document.write File.open(@filename,"w"), 2
    end
  end
end
