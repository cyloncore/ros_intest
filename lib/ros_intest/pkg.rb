module RosIntest
  module Pkg
    def Pkg.find_prefix pkg_name
      pkg_prefix, status = Open3.capture2e("ros2 pkg prefix #{pkg_name}")
      pkg_prefix.chomp!
      if pkg_prefix == 'Package not found'
        raise StandardError.new "Package '#{pkg_name}' not found."
      else
        return pkg_prefix
      end
    end
    def Pkg.find_share pkg_name
      return Pkg.find_prefix(pkg_name) + "/share/" + pkg_name
    end
  end
end
