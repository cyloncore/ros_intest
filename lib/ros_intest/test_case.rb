# Copyright 2024 Cyrille Berger.
#
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.

module RosIntest

  class TestCase
    attr_reader :ros_node, :name, :errors, :options
    def initialize ros_node, name, actions, options
      @ros_node = ros_node
      @name = name
      @actions = actions
      @errors = []
      @options = options
    end
    def start
      @actions.each do |act|
        act.prepare self
      end
      @actions.each do |act|
        act.execute self
      end
      @actions.each do |act|
        act.finish self
      end
    end
    def report_error err_msg
      @errors.append err_msg
    end
    def check_float msg, expected_value, value, tolerance
      if (value-expected_value).abs > tolerance
        self.report_error "#{msg}: expected #{expected_value} got #{value} with a tolerance of #{tolerance}."
      end
    end
    def check_regexp msg, regexp, value
      if regexp.match(value).nil?
        self.report_error "#{msg}: expected value '#{value}' to match /#{regexp.source}/." 
      end
    end
    def check_equals msg, ref, value
      if ref != value
        self.report_error "#{msg}: expected value '#{value}' to equal '#{ref}'." 
      end
    end
    def TestCase.create_from_yaml_file ros_node, fn, options
      test_description = RosIntest::YAML.load_file fn
      err_name = fn
      Utils.check_keys(err_name, test_description, ["name", "actions"])
      err_name = "#{fn} (#{test_description["name"]})"
      actions = []
      test_description["actions"].each do |act|
        if act.keys.size != 1
          raise StandardError.new "There should be one and only key to define an action, got '#{act.keys}' in #{err_name}"
        end
        type = act.keys[0]
        unless Actions.include? type
          raise StandardError.new "Unknown action type '#{type}' in #{err_name}"
        end
        actions.append Actions[type].create_from_dict err_name, act[type]
      end

      return TestCase.new ros_node, test_description['name'], actions, options
    end
  end
end
