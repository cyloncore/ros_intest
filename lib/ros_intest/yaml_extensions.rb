require 'yaml'

module RosIntest
  module YAML
    class Cat
      def initialize filename
        @filename = filename
      end
      def read_file base
        return File.read("#{base}/#{@filename}")
      end
    end
    def YAML.processYaml(filename, v)
      # puts v, v.is_a?(Cat), v.is_a?(Hash), v.is_a?(Array)
      if v.is_a? Cat
        return v.read_file File.dirname(filename)
      elsif v.is_a? Hash
        return v.map do |k,v|
          [k, processYaml(filename, v)]
        end.to_h
      elsif v.is_a? Array
        return v.map do |v|
          processYaml(filename, v)
        end.to_a
      else
        return v
      end
    end
    def YAML.load_file filename
      processYaml(filename, ::YAML.load_file(filename))
    end
  end
end

YAML.add_domain_type("", "cat") do |type, value|
  RosIntest::YAML::Cat.new value
end

YAML.add_domain_type("", "regexp") do |type, value|
  Regexp.new value
end

YAML.add_domain_type("", "eval") do |type, value|
  eval value
end