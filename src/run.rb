# Copyright 2024 Cyrille Berger.
#
# Use of this source code is governed by an MIT-style
# license that can be found in the LICENSE file or at
# https://opensource.org/licenses/MIT.

require 'rclrb'
require 'ros_intest'
require 'std_msgs/msg'
require 'optparse'

class TestRunner < Rclrb::Node
  attr_reader :error_code
  def initialize args
    @error_code = 0
    super "test_runner"
    @t = Thread.new do
      reporters = []
      options = {
        :verbose => false
      }
      OptionParser.new do |opt|
        opt.on('--output-junit FILENAME') { |o| reporters.append(RosIntest::JunitReporter.new o) }
        opt.on('--verbose') { |o| options[:verbose] = true }
      end.parse! args
      if args.length == 0
        raise StandardError.new "Invalid number of arguments, expected at least one test case."
      end
      
      start_time_all = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      begin
        i = 0
        args.each do |tc|
          begin
            # Run case
            c = RosIntest::TestCase.create_from_yaml_file self, tc, options
            # Report to reporters
            start_time_test_case = Process.clock_gettime(Process::CLOCK_MONOTONIC)
            reporters.each() do |rep|
              rep.start_case tc, c.name
            end
            puts "Run case '#{c.name}' (#{i+=1}/#{args.length})"
            c.start()
            unless c.errors.empty?
              c.errors.each do |err_msg|
                puts " - #{err_msg}"
                reporters.each() do |rep|
                  rep.report_error err_msg
                end
              end
              @error_code = 255
            end
            # Report to reporters
            end_time_test_case = Process.clock_gettime(Process::CLOCK_MONOTONIC)
            reporters.each() do |rep|
              rep.end_case end_time_test_case-start_time_test_case
            end
          end
        rescue YAML::SyntaxError => e
          puts "Failed to load test from #{tc} with error: #{e.message}."
          reporters.each() do |rep|
            rep.report_parse_error tc, e.message
          end
          @error_code = 255
        end
      rescue StandardError => e
        puts "Backtrace:\n\n#{e.backtrace.join "\n"}\n\nAn error occured: #{e.message}"
        @error_code = 255
      end
      end_time_all = Process.clock_gettime(Process::CLOCK_MONOTONIC)
      reporters.each() do |rep|
        rep.close end_time_all-start_time_all
      end
      
      Rclrb.shutdown
    end
  end
end

def main(args)
  Rclrb.init arguments: args
  tr = TestRunner.new ARGV
  Rclrb.spin tr
  exit(tr.error_code)
end
