ros_intest
==========

This package is intended to ease

cmake
-----

It provides one function:

* `ros_intest_add_test` add a ros_intest test case.

setup.py
--------

To define `ros_intest` tests in Python package that uses `setup.py`, it is enough to add a test file starting with `test_` and with extension `.ros_intest.yaml`, for instance, `test_running_system.ros_intest.yaml`.
